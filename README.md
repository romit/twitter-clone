# Twitter Clone App
A simple Twitter Clone App.

> **Note** : The changes in data from twitter API might take some time to reflect. That's because twitter keeps data in different databases for API.

#### Features in the app

- Authenticate user using Twitter API
- List user timeline tweets
- Post a new tweet (Only supports text for now)
- Reply to a tweet
- Retweet a tweet, unretweet
- Mark a tweet as favorite, unfavorite

#### Get project code

- Run following command in your terminal : *git clone https://gitlab.com/romit/twitter-clone.git*
- Move to /twitter-clone dir

#### How to run

##### Using Docker

The root dir contains *docker-compose.yml* file, which installs all the necessary packages to run this project.
To run this project using docker, [install docker community edition](https://www.docker.com/get-docker), if you don't have it installed already.

- Once you have docker installed on your system, open terminal and run *cd /path/to/twitter-clone*.
- In the root dir (/twitter-clone) run *docker-compose up*
- Once it installs all the packages and starts the server, open [localhost:3000](http://localhost:3000) in your browser

##### Using npm

For this, you need to have **nodejs** and **npm** installed on you system.

- To install nodejs and npm, visit [https://www.npmjs.com/get-npm](https://www.npmjs.com/get-npm)

Once you have nodejs, and npm ready on your system, do following:

- move to */path/to/twitter-clone* inside your terminal, now this is your root dir

##### Start the web app

- Inside root dir, run **npm install**
- After installing packages, run **npm start**. This will start the website, which can be accessed at [localhost:3000](http://localhost:3000)


### For any help regarding the project and it's installation contact me at [choudharyromit@gmail.com](mailto:choudharyromit@gmail.com)
