import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import Routes from "./Routes";

var Utils = require('./utils.js');

class App extends Component {

  constructor(props) {
    super(props);

    this.state = {
      isAuthenticated: false,
      isAuthenticating: true
    };

  }

  async componentDidMount() {
    try {
      this.userHasAuthenticated(Utils.store('auth_token').length!==0 && Utils.store('user_id').length!==0);
    }
    catch(e) {
      if (e !== 'No current user') {
        alert(e);
      }
    }

    this.setState({ isAuthenticating: false });
  }

  userHasAuthenticated = authenticated => {
    this.setState({ isAuthenticated: authenticated });
  }

  render() {
    
    const childProps = {
      isAuthenticated: this.state.isAuthenticated,
      userHasAuthenticated: this.userHasAuthenticated
    };

    return (
      !this.state.isAuthenticating &&
      <div className="App">
        <Routes childProps={childProps} />
      </div>
    );
  }
}

export default withRouter(App);