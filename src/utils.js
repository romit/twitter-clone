export function store (namespace, data) {
	if (data) {
		return localStorage.setItem(namespace, JSON.stringify(data));
	}

	var store = localStorage.getItem(namespace);
	return (store && JSON.parse(store)) || [];
}

export function clearStore(namespace){
	localStorage.clear(namespace);
}

export function logoutUser(){
	localStorage.clear();
}

export function extend () {
	var newObj = {};
	for (var i = 0; i < arguments.length; i++) {
		var obj = arguments[i];
		for (var key in obj) {
			if (obj.hasOwnProperty(key)) {
				newObj[key] = obj[key];
			}
		}
	}
	return newObj;
}

export function getParameterByName(name, url) {
  if (!url) url = window.location.href;
  name = name.replace(/[[\]]/g, "\\$&");
  var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
      results = regex.exec(url);
  if (!results) return null;
  if (!results[2]) return '';
  return decodeURIComponent(results[2].replace(/\+/g, " "));
}

export function getRichTweetText(tweetText, character){
	
	tweetText = tweetText.replace(/(?:\r\n|\r|\n)/g, ' ');

	var richTweetText = '';
	var linkArr = {'@': 'https://twitter.com/',
				   '#': 'https://twitter.com/hashtag/' };

	var startIndex = 0;
	var indexOfAt = tweetText.indexOf(character, startIndex);
	var indexOfAtTextEnd = tweetText.indexOf(' ', indexOfAt);

	while(indexOfAt!==-1 && indexOfAt!==indexOfAtTextEnd && startIndex!==-1){
		richTweetText += tweetText.substring(startIndex, indexOfAt);
		if(indexOfAtTextEnd===-1){
			richTweetText += "<a target='_blank' href=" + linkArr[character] + tweetText.substring(indexOfAt+1) + " >" + tweetText.substring(indexOfAt) + "</a>";	
		}
		else{
			richTweetText += "<a target='_blank' href=" + linkArr[character] + tweetText.substring(indexOfAt+1, indexOfAtTextEnd) + " >" + tweetText.substring(indexOfAt, indexOfAtTextEnd) + "</a>";
		}
		

		startIndex = indexOfAtTextEnd;
		indexOfAt = tweetText.indexOf(character, startIndex);
		indexOfAtTextEnd = tweetText.indexOf(' ', indexOfAt);
	}

	if(startIndex!==-1){
		richTweetText += tweetText.substring(startIndex);
	}

	return richTweetText !== '' ? richTweetText : tweetText;
}

export function urlify(text) {
    var urlRegex = /(https?:\/\/[^\s]+)/g;
    return text.replace(urlRegex, function(url) {
        return '<a target="_blank" href="' + url + '">' + url + '</a>';
    })
}