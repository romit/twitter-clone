import React, { Component } from "react";
import "./TweetItem.css";

import TweetMetaItem from './TweetMetaItem';
import TweetActionsItem from './TweetActionsItem';

export default class TweetItem extends Component{

	constructor(props){
		super(props);

		this.toggleComplete = this.toggleComplete.bind(this);
		this.onTweetActionClick = this.onTweetActionClick.bind(this);
	}

	onTweetActionClick (tweetActionType) {
		this.props.onTweetActionClick(this.props.tweetItem, tweetActionType);
		return;
	}

	toggleComplete (e) {
		e.preventDefault();
		this.props.toggleComplete(this.props.nodeId);
		return;
	}

	render() {
		var classes = 'list-group-item clearfix';
		var media = this.props.tweetItem.entities.media;

		return (
			<li className={classes}>
				
				<TweetMetaItem tweetItem={this.props.tweetItem} />

				{ media!==undefined &&
					<img src={media[0].media_url} className='tweet-media' alt='tweet-media'/>
				}
				
				<TweetActionsItem tweetItem={this.props.tweetItem} onTweetActionClick={this.onTweetActionClick}/>

			</li>
		);
	}
}