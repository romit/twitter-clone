import React, { Component } from "react";

var Utils = require('../utils');

export default class TweetMetaItem extends Component{
	render(){

		var userProfileLink = 'https://twitter.com/' + this.props.tweetItem.user.screen_name;

		var richTweetText = Utils.urlify(this.props.tweetItem.text);
		richTweetText = Utils.getRichTweetText(richTweetText, '#');
		richTweetText = Utils.getRichTweetText(richTweetText, '@');
		var finalRichTweetText = { __html: richTweetText};

		return (
			<div className={this.props.className}>
				<div className="pull-left" role="group">
					<img src={this.props.tweetItem.user.profile_image_url.replace('normal', 'bigger')} alt='user' className='tweet-user-image' />
				</div>
				<div className='tweet-center-content'>
					<p className='tweet-user-data'>
						<a href={userProfileLink} target='_blank'><span className='tweet-user-fullname'> {this.props.tweetItem.user.name} </span></a>&nbsp;
						{ this.props.tweetItem.user.verified &&
							<img src='../../verified.png' className='tweet-more-options-icon' alt='verified'/>
						}
						<a href={userProfileLink} target='_blank' className='anchor-without-underline'><span className='tweet-user-username'> @{this.props.tweetItem.user.screen_name} </span></a>
					</p>
					<p className='tweet-text' dangerouslySetInnerHTML={finalRichTweetText}></p>
				</div>
			</div>
		);
	}
}