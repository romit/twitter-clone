import React, { Component } from "react";
import TwitterAppHome from './TwitterAppHome';
import "./Home.css";

export default class Home extends Component {
  
  render() {
    return (
        <TwitterAppHome userHasAuthenticated={this.props.userHasAuthenticated} isAuthenticated={this.props.isAuthenticated} history={this.props.history}/>
    );
  }
}