import React, { Component } from "react";
import LoaderButton from "../components/LoaderButton";
import "./Login.css";

var Codebird = require("codebird");
var Utils = require('../utils');

export default class Login extends Component {
  constructor(props) {
    super(props);
    
    this.state = {
      isLoading: false,
      email: "",
      password: ""
    };
  }

  componentDidMount(){

    if(this.props.isAuthenticated){
      this.props.history.push('/');
      return;
    }

    var that = this;

    var oauth_token = Utils.getParameterByName('oauth_token', window.location);
    var oauth_verifier = Utils.getParameterByName('oauth_verifier', window.location);
    if(oauth_token!=null && oauth_token!==''){

      var cb = new Codebird();
      cb.setConsumerKey("UufEWUkdYy5bg1JjUSioFy5LI", "8gOrV5VEJSOeAVoL8WycNdXojKq2Z0bJ8MsmyY7fNx5ZYAPRCW");
      cb.setToken(oauth_token, Utils.store('auth_token_secret'));

      cb.__call(
          "oauth_accessToken",
          {
            oauth_verifier: oauth_verifier
          },
          function (reply,rate,err) {
            if(reply && reply.httpstatus === 200){
              Utils.store('auth_token', reply.oauth_token);
              Utils.store('auth_token_secret', reply.oauth_token_secret);
              Utils.store('user_screen_name', reply.screen_name);
              Utils.store('user_id', reply.user_id);
              that.props.userHasAuthenticated(true);
              that.props.history.push('/');
            }
            else{
              alert('Failed to login');
            }
      });
    }
  }

  validateForm() {
    return this.state.email.length > 0 && this.state.password.length > 0;
  }

  handleChange = event => {
    this.setState({
      [event.target.id]: event.target.value
    });
  }

  handleSubmit = async event => {

    var that = this;
    event.preventDefault();
    this.setState({ isLoading: true });

    if(this.props.isAuthenticated){
      return;
    }
    else{
      var cb = new Codebird();
      cb.setConsumerKey("UufEWUkdYy5bg1JjUSioFy5LI", "8gOrV5VEJSOeAVoL8WycNdXojKq2Z0bJ8MsmyY7fNx5ZYAPRCW");

      cb.__call(
          "oauth_requestToken",
          function (reply,rate,err) {
              if (err) {
                  console.log("error response or timeout exceeded" + err.error);
              }
              if (reply) {
                  // stores it
                  cb.setToken(reply.oauth_token, reply.oauth_token_secret);
                  Utils.store('auth_token', reply.oauth_token);
                  Utils.store('auth_token_secret', reply.oauth_token_secret);

                  // gets the authorize screen URL
                  cb.__call(
                      "oauth_authorize",
                      {},
                      function (auth_url) {
                          window.codebird_auth = window.open(auth_url, "_self");
                          that.setState({ isLoading: false });
                      }
                  );
              }
          }
      );
    }
  }

  render() {
    return (
      <div className='row login-block'>
        <div className="Login col-xs-6 col-xs-offset-3 col-sm-4 col-sm-offset-4 col-md-4 col-md-offset-4">
          <img src='../../twitter-logo-white.png' alt='twitter-logo' className='twitter-logo text-center'/>
          <h2>See what’s happening in the world right now</h2>
          <h4>Join Twitter today.</h4>
          <LoaderButton
            className='twitter-login-button'
            type="button"
            isLoading={this.state.isLoading}
            text="Get Started"
            onClick={this.handleSubmit}
            loadingText="Logging in…"
          />
        </div>
      </div>
    );
  }
}