import React, { Component } from "react";
import NewTweetForm from './NewTweetForm';
import "./TweetForm.css";

export default class TweetForm extends Component{

	constructor(props){
		super(props);
		this.state = {
			value: ''
		};

		this.doSubmit = this.doSubmit.bind(this);
		this.handleChange = this.handleChange.bind(this);
	}

	handleChange(event) {
	    this.setState({value: event.target.value});
	  }

	doSubmit (tweet) {
		this.props.onTweetSubmit(tweet);
	}

	render() {
		return (
			<div className="commentForm vert-offset-top-2">
				<div className="clearfix">
					<form className="todoForm form-horizontal" onSubmit={this.doSubmit}>
						<div className="form-group">
								<div className='col-md-1'>
									<img src={this.props.userImage} className='new-tweet-user-image' alt='user'/>
								</div>
								<NewTweetForm className='col-md-11' isCollapsed={this.props.isCollapsed} onTweetSubmit={this.doSubmit} isToggleAllowed={true} hintText="What's happening?" minTextLength={1}/>
						</div>
					</form>
				</div>
			</div>
		);
	}
}