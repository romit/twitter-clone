import React, { Component } from 'react';
import LoaderButton from "../components/LoaderButton";

export default class NewTweetForm extends Component{

	constructor(props){
		super(props);

		this.state = {
			value: '',
			isCollapsed: props.isCollapsed,
			isToggleAllowed: props.isToggleAllowed,
			isLoading: false
		};

		this.handleChange = this.handleChange.bind(this);
		this.doSubmit = this.doSubmit.bind(this);
		this.expandView = this.expandView.bind(this);
		this.collapseView = this.collapseView.bind(this);
	}

	handleChange(event){
		this.setState({value: event.target.value});
	}

	collapseView(){
		if(this.state.isToggleAllowed && this.state.value===''){
			this.setState({
				isCollapsed: true
			})
		}
	}

	expandView(){
		if(this.state.isToggleAllowed && this.state.value===''){
			this.setState({
				isCollapsed: false
			})
		}
	}

	doSubmit(){
		var tweet = this.state.value;
		if (!tweet && this.props.minTextLength===1) {
			return;
		}
		
		this.props.onTweetSubmit(tweet);
		this.setState({value:''});
	}

	render(){

		var rowCount = 1;
		if(!this.state.isCollapsed){
			rowCount = 4;
		}

		var charLimitText = (this.state.value.length) + '/140';

		return (
			<div className={this.props.className}>
				<textarea maxLength="140" rows={rowCount} type="text" value={this.state.value} id="task" ref="task" className="new-task-input" placeholder={this.props.hintText} onChange={this.handleChange} onFocus={this.expandView} onBlur={this.collapseView}/>
				{ !this.state.isCollapsed &&
					<div>
						<span className='tweet-limit'>{charLimitText}</span>
						<LoaderButton
				            className='pull-right post-new-tweet'
				            type="button"
				            disabled={this.state.value.length<this.props.minTextLength}
				            isLoading={this.state.isLoading}
				            text="Tweet"
				            onClick={this.doSubmit}
				            loadingText="Tweet"
				          />
				    </div>
				}
			</div>
		);
	}
}