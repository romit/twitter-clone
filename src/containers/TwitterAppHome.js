import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Navbar, Nav, NavItem } from "react-bootstrap";
import Modal from './Modal';
import NewTweetForm from './NewTweetForm';
import LoaderButton from "../components/LoaderButton";
import TweetList from './TweetList';
import TweetForm from './TweetForm';

import TweetMetaItem from './TweetMetaItem';
import "./TwitterAppHome.css";

var Codebird = require("codebird");
var Utils = require('../utils');
var cb;

export default class TwitterAppHome extends Component{

	constructor(props){
		super(props);

		this.state = {
			data: [],
			userData: {},
			isLoading: false,
			isReplyDialogOpen: false,
			currentTweet: {}
		};

		this.handlePostNewTweet = this.handlePostNewTweet.bind(this);
		this.handleLogout = this.handleLogout.bind(this);
		this.loadMoreTweets = this.loadMoreTweets.bind(this);

		this.onTweetActionClick = this.onTweetActionClick.bind(this);
		this.toggleReplyTweetModal = this.toggleReplyTweetModal.bind(this);
		
		this.handleTweetReply = this.handleTweetReply.bind(this);
		this.toggleRetweetStatus = this.toggleRetweetStatus.bind(this);
		this.toggleTweetFavoriteState = this.toggleTweetFavoriteState.bind(this);
	}

	toggleReplyTweetModal(e){
		console.log(e.target.id);
		
		if(e.target.id === 'dialog-outer-div' || e.target.id === 'dialog-close'){
			this.setState({
		      isReplyDialogOpen: !this.state.isReplyDialogOpen
		    });
		}

	    if(!this.state.isReplyDialogOpen){
	    	this.setState({
	    		currentTweet: {}
	    	})
	    }
	}

	onTweetActionClick(tweet, tweetActionType){
		switch(tweetActionType){
			case 'reply':
				this.setState({
					currentTweet: tweet,
					isReplyDialogOpen: !this.state.isReplyDialogOpen
				});
				break;
			case 'retweet':
				this.setState({
					currentTweet: tweet,
				}, () => {
					this.toggleRetweetStatus();
				});
				break;
			case 'favorite':
				this.setState({
					currentTweet: tweet
				}, () => {
					this.toggleTweetFavoriteState();
				});
				break;
			default:
				break;
		}
	}

	toggleTweetFavoriteState(){
		var that = this;
		var params = {
			id: this.state.currentTweet.id_str
		};

		var apiCallName;
		if(this.state.currentTweet.favorited){
			apiCallName = 'favorites_destroy';
		}
		else{
			apiCallName = 'favorites_create';
		}

		cb.__call(
		    apiCallName,
		    params,
		    function (reply, rate, err) {
		        if(reply && reply.httpstatus === 200){
		       		that.state.currentTweet.favorited = !that.state.currentTweet.favorited;
		       		that.setState({
		       			currentTweet: {}
		       		})
		       }
		       else{
		       		alert("Failed to mark tweet as favorite.");
		       }
		    }
		);
	}

	handleTweetReply(repliedTweetText){

		var that = this;

		var params = {
			id: this.state.currentTweet.id_str,
			in_reply_to_status_id: this.state.currentTweet.id_str,
		    status: '@' + this.state.currentTweet.user.screen_name + ' ' + repliedTweetText
		};

		cb.__call(
		    "statuses_update",
		    params,
		    function (reply, rate, err) {
		    	that.setState({
				    isReplyDialogOpen: !that.state.isReplyDialogOpen
				});

			    if(!that.state.isReplyDialogOpen){
			    	that.setState({
			    		currentTweet: {}
			    	})
			    }
		        if(reply && reply.httpstatus === 200){
		        	alert('Replied to the tweet successfully');
		       	}
		       	else{
			    	alert('Failed to reply. Please try again.');
			    }
		    }
		);
	}

	toggleRetweetStatus(repliedTweetText){

		var that = this;

		var apiCallName;
		if(this.state.currentTweet.retweeted){
			apiCallName = 'statuses_unretweet_ID';
		}
		else{
			apiCallName = 'statuses_retweet_ID';
		}

		cb.__call(
		    apiCallName,
		    "id="+this.state.currentTweet.id_str,
		    function (reply, rate, err) {
		        if(reply && reply.httpstatus === 200){

		       		that.state.currentTweet.retweeted = !that.state.currentTweet.retweeted;
			    	that.setState({
			    		currentTweet: {}
			    	})
		       }
		       else{
		       		alert("Failed to retweet.");
		       }
		    }
		);
	}

	componentDidMount(){

		if(!this.props.isAuthenticated){
	      this.props.history.push('/login');
	      return;
	    }

		var that = this;

		var auth_token = Utils.store('auth_token');
		var auth_token_secret = Utils.store('auth_token_secret');

		if(auth_token!=null && auth_token!=='' && auth_token_secret!=null && auth_token_secret!==''){

			cb = new Codebird();
			cb.setConsumerKey("UufEWUkdYy5bg1JjUSioFy5LI", "8gOrV5VEJSOeAVoL8WycNdXojKq2Z0bJ8MsmyY7fNx5ZYAPRCW");
			cb.setToken(auth_token, auth_token_secret);

			var params = {
			    screen_name: Utils.store('user_screen_name')
			};
			cb.__call(
			    "users_show",
			    params,
			    function (reply, rate, err) {
			    	if(reply && reply.httpstatus === 200){
			        	that.setState({
			        		userData: reply
			        	})
			        }
			        else{
			        	console.log(err);
			        }
			    }
			);

			cb.__call(
			    "statuses_homeTimeline",
			    {},
			    function (reply, rate, err) {
			    	if(reply && reply.httpstatus === 200){
				        that.setState({
				        	data: reply
				        })
				    }
				    else{
				    	alert('Failed to load tweets.');
				    }
			    }
			);
		}
	}

	handlePostNewTweet (tweet) {

		var that = this;

		var params = {
		    status: tweet
		};

		cb.__call(
		    "statuses_update",
		    params,
		    function (reply, rate, err) {

		        if(reply && reply.httpstatus === 200){
		        	that.setState({
		        		data: [reply].concat(that.state.data)
		        	})
		        }
		        else{
		        	alert('Failed to post tweet.')
		        }
		    }
		);
	}

	loadMoreTweets(){

		this.setState({ isLoading: true });
		var that = this;

		var params = {
		    max_id: this.state.data[this.state.data.length-1].id
		};

		cb.__call(
		    "statuses_homeTimeline",
		    params,
		    function (reply, rate, err) {
		    	if(reply && reply.httpstatus === 200){
			        that.setState({
			        	data: that.state.data.concat(reply.splice(1)),
			        	isLoading: false
			        })
			    }
		    }
		);

	}

	async handleLogout(event){
	    Utils.logoutUser();
	    this.props.userHasAuthenticated(false);
	    this.props.history.push("/login");
	}

	render() {

		return (
			<div className="Home">
				<Navbar fluid collapseOnSelect>
		          <Navbar.Header>
		            <Navbar.Brand>
		              <Link to="/">Home</Link>
		            </Navbar.Brand>
		            <Navbar.Toggle />
		          </Navbar.Header>
		          <Navbar.Collapse>
		            <Nav pullRight>
		              <NavItem className='nav-item-user-image'> <img src={this.state.userData.profile_image_url} alt='user' className='navbar-user-image' /> </NavItem>	
		              <NavItem onClick={this.handleLogout}>Logout</NavItem>
		            </Nav>
		          </Navbar.Collapse>
		        </Navbar>
				<div className='row twitter-timeline-content'>
					<div className="col-md-6 col-md-offset-3">
						<TweetForm onTweetSubmit={this.handlePostNewTweet} userImage={this.state.userData.profile_image_url}  isCollapsed={true}/>
						<TweetList data={this.state.data} onTweetActionClick={this.onTweetActionClick}/>
						{ this.state.data.length !== 0 &&
							<LoaderButton
					            className='load-more-tweets'
					            type="button"
					            disabled={this.state.isLoading}
					            isLoading={this.state.isLoading}
					            text="Load more tweets"
					            onClick={this.loadMoreTweets}
					            loadingText="Loading..."
					          />
						}
					</div>
				</div>

		        <Modal show={this.state.isReplyDialogOpen}
		          onClose={this.toggleReplyTweetModal}>
		          	<div className='tweet-reply-modal-content-div'>
					    <h4>Add another tweet</h4>
					    <TweetMetaItem tweetItem={this.state.currentTweet} className='extra-padding'/>
					    <NewTweetForm className='col-md-12 tweet-input-box' isCollapsed={false} isToggleAllowed={false} hintText='Add another tweet' onTweetSubmit={this.handleTweetReply} minTextLength={1}/>
					</div>
		        </Modal>


			</div>
		);
	}
}