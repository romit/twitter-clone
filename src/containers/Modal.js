import React from 'react';
import PropTypes from 'prop-types';

class Modal extends React.Component {

  render() {
    // Render nothing if the "show" prop is false
    if(!this.props.show) {
      return null;
    }

    // The gray background
    const backdropStyle = {
      position: 'fixed',
      top: 0,
      bottom: 0,
      left: 0,
      right: 0,
      backgroundColor: 'rgba(0,0,0,0.6)',
      padding: 50,
      zIndex: 1000
    };

    // The modal "window"
    const modalStyle = {
      backgroundColor: '#fff',
      borderRadius: 5,
      maxWidth: 600,
      minHeight: 200,
      width: 600,
      margin: '0 auto',
      padding: 0,
      position: 'relative'
    };

    return (
      <div id='dialog-outer-div' className="backdrop" style={backdropStyle} onClick={this.props.onClose}>
        <div id='dialog-inner-div' className="model" style={modalStyle}>
          {this.props.children}

          <span id='dialog-close' onClick={this.props.onClose} className='dialog-close-button'>
            &#x2715;
          </span>
        </div>
      </div>
    );
  }
}

Modal.propTypes = {
  onClose: PropTypes.func.isRequired,
  show: PropTypes.bool,
  children: PropTypes.node
};

export default Modal;