import React, { Component } from "react";
import TweetItem from './TweetItem';

export default class TweetList extends Component{

	constructor(props){
		super(props);

		this.onTweetActionClick = this.onTweetActionClick.bind(this);
	}

	onTweetActionClick (tweetItem, tweetActionType) {
		this.props.onTweetActionClick(tweetItem, tweetActionType);
		return;
	}

	render() {
		var listNodes = this.props.data.map(function (listItem) {
			return (
				<TweetItem key={listItem.id_str} nodeId={listItem.id_str} tweetItem={listItem} onTweetActionClick={this.onTweetActionClick}/>
			);
		},this);
		return (
			<ul className="list-group">
				{listNodes}
			</ul>
		);
	}
}