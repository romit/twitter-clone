import React, { Component } from "react";
import './TweetActionsItem.css';

export default class TweetActionsItem extends Component{

	constructor(props){
		super(props);

		this.replyTweet = this.replyTweet.bind(this);
		this.retweet = this.retweet.bind(this);
		this.markTweetFavorite = this.markTweetFavorite.bind(this);
	}

	replyTweet(){
		this.props.onTweetActionClick('reply');
	}

	retweet(){
		this.props.onTweetActionClick('retweet');
	}

	markTweetFavorite(){
		this.props.onTweetActionClick('favorite');
	}

	render(){

		var retweetClassName = 'tweet-options-item retweet';
		var favoriteClassName = 'tweet-options-item favorite';

		if(this.props.tweetItem.retweeted){
			retweetClassName += ' active';
		}

		if(this.props.tweetItem.favorited){
			favoriteClassName += ' active';
		}

		return (
			<div className='tweet-options'>
				<div className='tweet-options-item reply' onClick={this.replyTweet}>
					<img src='../../reply.png' alt='reply'/>
					<span> Reply </span>
				</div>
				<div className={retweetClassName} onClick={this.retweet}>
					<img src='../../retweet.png' alt='retweet'/>
					<span> { this.props.tweetItem.retweet_count} </span>
				</div>
				<div className={favoriteClassName} onClick={this.markTweetFavorite}>
					<img src='../../favorite.png' alt='favorite'/>
					<span> { this.props.tweetItem.favorite_count} </span>
				</div>
			</div>
		);
	}
}